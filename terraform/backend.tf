terraform {
  backend "s3" {
    bucket = "lex-build-landing-aws-jenkins-terraform"
    key = "lex-build-landing-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}